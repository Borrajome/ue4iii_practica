// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "UE4III_borrajomeGameMode.h"
#include "UE4III_borrajomeCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUE4III_borrajomeGameMode::AUE4III_borrajomeGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
