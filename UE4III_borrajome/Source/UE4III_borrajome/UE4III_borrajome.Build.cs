// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class UE4III_borrajome : ModuleRules
{
    public UE4III_borrajome(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });

        PrivateDependencyModuleNames.AddRange(new string[] { });
        var basePath = Path.GetDirectoryName(RulesCompiler.GetFileNameFromType(GetType()));
        string thirdPartyPath = Path.Combine(basePath, "..", "..", "Thirdparty");


        //FMOD
        PublicIncludePaths.Add(Path.Combine(thirdPartyPath, "FMOD", "Includes"));



        PublicLibraryPaths.Add(Path.Combine(thirdPartyPath, "FMOD", "Libraries", "Win64"));
        PublicAdditionalLibraries.Add("fmod64_vc.lib");
        string fmodDllPath = Path.Combine(thirdPartyPath, "FMOD", "Libraries", "Win64", "fmod64.dll");
        RuntimeDependencies.Add(new RuntimeDependency(fmodDllPath));

        PublicLibraryPaths.Add(Path.Combine(thirdPartyPath, "FMOD", "Libraries", "Win64"));
        PublicAdditionalLibraries.Add("fmodstudio64_vc.lib");
        string fmodstudioDllPath = Path.Combine(thirdPartyPath, "FMOD", "Libraries", "Win64", "fmodstudio64.dll");
        RuntimeDependencies.Add(new RuntimeDependency(fmodstudioDllPath));

        string binariesDir = Path.Combine(basePath, "..", "..", "Binaries", "Win64");
        if (!Directory.Exists(binariesDir))
            System.IO.Directory.CreateDirectory(binariesDir);

        string fmodDllDest = System.IO.Path.Combine(binariesDir, "fmod64.dll");
        CopyFile(fmodDllPath, fmodDllDest);
        PublicDelayLoadDLLs.AddRange(new string[] { "fmod64.dll" });

        string fmodstudioDllDest = System.IO.Path.Combine(binariesDir, "fmodstudio64.dll");
        CopyFile(fmodstudioDllPath, fmodstudioDllDest);
        PublicDelayLoadDLLs.AddRange(new string[] { "fmodstudio64.dll" });


    }


    private void CopyFile(string source, string dest)
    {
        System.Console.WriteLine("Copying {0} to {1}", source, dest);
        if (System.IO.File.Exists(dest))
        {
            System.IO.File.SetAttributes(dest, System.IO.File.GetAttributes(dest) & ~System.IO.FileAttributes.ReadOnly);
        }
        try
        {
            System.IO.File.Copy(source, dest, true);
        }
        catch (System.Exception ex)
        {
            System.Console.WriteLine("Failed to copy file: {0}", ex.Message);
        }
    }
}

