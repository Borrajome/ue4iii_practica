// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/CapsuleComponent.h"
#include "DanceCharacter.generated.h"

UCLASS()
class UE4III_BORRAJOME_API ADanceCharacter : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADanceCharacter();

	///SkeletalMeshComponent of the Enemy
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		USkeletalMeshComponent *MyMesh;

	///CapsuleComponent of the Enemy
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UCapsuleComponent *MyCapsule;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
