// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AudioAPI.h"
#include <memory.h>
#include <string.h>

#include "UObject/NoExportTypes.h"



#include "AudioManager.generated.h"



/**
 * 
 */
UCLASS()
class UE4III_BORRAJOME_API UAudioManager : public UObject
{
	GENERATED_BODY()
	
public:
	UAudioManager();
	~UAudioManager();

	UFUNCTION()
		void init();


	void playSound(const std::string& filename, bool loop, bool d3, const FVector sound_pos, float volume);


	UFUNCTION()
		void set3DListener(const FVector listenerpos, const FVector forward, const FVector up);

	UFUNCTION()
		void update();



private:

	std::unique_ptr<AudioAPI> _soundManager;
};
