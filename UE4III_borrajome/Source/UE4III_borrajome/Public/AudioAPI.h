// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <string>
#include <map>
#include <vector>

#include "fmod.hpp"
#include "fmod_studio.hpp"

#include "Engine.h"
#include "Math/Vector.h"
#include "CoreMinimal.h"

/**
 * 
 */
class UE4III_BORRAJOME_API AudioAPI
{
public:
	AudioAPI();
	~AudioAPI();

	void init();
	void update();
	//void shutdown();
	int errorCheck(FMOD_RESULT result, int id);

	void loadSound(const std::string& filename, bool loop, bool d3);
	void playSound(const std::string& filename, const FVector sound_pos, float volume);
	void loadBank(const std::string& filename);
	void loadEvent(const std::string& filename);
	void playEvent(const std::string& filename);
	void stopEvent(const std::string& filename, bool fade_out);
	bool isEventPlaying(const std::string& filename);
	void getEventParameter(const std::string& filename, const std::string& parameter_name, float *parameter);
	void setEventParameter(const std::string& filename, const std::string& parameter_name, float parameter_value);
	void set3DListener(const FVector listenerpos, const FVector forward, const FVector up);

	FMOD_VECTOR FVectorToFMOD(const FVector vector);
	void setChannel3DAttribs(int channel_id, const FVector sound_pos, const FVector sound_velocity);

private:
	//TODO: set3DMinMaxDistance!!!!!!
	FMOD::Studio::System *studio_system_;
	FMOD::System *low_system_;

	int next_channel_id_;
	const float DISTANCEFACTOR = 100.0f; //Units per meter centimeters

	typedef std::map<std::string, FMOD::Sound*> Msounds_;
	typedef std::map<int, FMOD::Channel*> Mchannels_;
	typedef std::map<int, FMOD::ChannelGroup*> Mchannels_grp_;
	typedef std::map<std::string, FMOD::Studio::EventInstance*> Mevents_;
	typedef std::map<std::string, FMOD::Studio::Bank*> Mbanks_;

	Msounds_ sounds_;
	Mchannels_ channels_;
	Mchannels_grp_ channels_grp_;
	Mevents_ events_;
	Mbanks_ banks_;
};
