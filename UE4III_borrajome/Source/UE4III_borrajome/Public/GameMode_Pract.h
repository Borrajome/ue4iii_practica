// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "Engine.h"
#include "AudioManager.h"
#include "GameMode_Pract.generated.h"

/**
 * 
 */
UCLASS()
class UE4III_BORRAJOME_API AGameMode_Pract : public AGameMode
{
	GENERATED_BODY()

public:

	AGameMode_Pract();

	UPROPERTY(EditAnywhere, Category = Mesh)
		class UAudioManager*  audio_comp;
	
public:

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;
	
};
