// Fill out your copyright notice in the Description page of Project Settings.

#include "GameMode_Pract.h"

AGameMode_Pract::AGameMode_Pract() {

	audio_comp = CreateDefaultSubobject<UAudioManager>(TEXT("AudioComp"));
}


void AGameMode_Pract::BeginPlay()
{
	Super::BeginPlay();

	audio_comp->init();

	FString songsPath = FPaths::ProjectContentDir() + "Audio/";

	std::string song_path(TCHAR_TO_UTF8(*songsPath));

	song_path = song_path + "Track1.wav";

	FVector pos = { 850.0, 30.0, 600.0 };

	audio_comp->playSound(song_path, true, true, pos, 0.5f);

}

void AGameMode_Pract::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	audio_comp->update();

}