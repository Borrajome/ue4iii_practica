// Fill out your copyright notice in the Description page of Project Settings.

#include "DanceCharacter.h"


// Sets default values
ADanceCharacter::ADanceCharacter()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyMesh = CreateDefaultSubobject<USkeletalMeshComponent>("Mesh");

	MyCapsule = CreateDefaultSubobject<UCapsuleComponent>("Capsule");
	RootComponent = MyCapsule;

	MyCapsule->InitCapsuleSize(42.f, 96.0f);

	MyMesh->SetupAttachment(MyCapsule);

	MyMesh->SetNotifyRigidBodyCollision(true);
}

// Called when the game starts or when spawned
void ADanceCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADanceCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

