// Fill out your copyright notice in the Description page of Project Settings.

#include "AudioManager.h"
#include <string>


UAudioManager::UAudioManager()
{
}

UAudioManager::~UAudioManager()
{
}

void UAudioManager::init() {
	_soundManager = std::unique_ptr<AudioAPI>(new AudioAPI());
	_soundManager->init();
}

void UAudioManager::playSound(const std::string& filename, bool loop, bool d3, const FVector sound_pos, float volume) {

	_soundManager->loadSound(filename, loop, d3);
	_soundManager->playSound(filename, sound_pos, volume);
}


void UAudioManager::set3DListener(const FVector listenerpos, const FVector forward, const FVector up) {

	_soundManager->set3DListener(listenerpos, forward, up);

}

void UAudioManager::update() {
	_soundManager->update();
}

