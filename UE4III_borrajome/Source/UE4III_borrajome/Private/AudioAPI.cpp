
// Fill out your copyright notice in the Description page of Project Settings.

#include "AudioAPI.h"

AudioAPI::AudioAPI()
{
	studio_system_ = nullptr;
	low_system_ = nullptr;
}

AudioAPI::~AudioAPI()
{
	AudioAPI::errorCheck(studio_system_->unloadAll(), 3);
	AudioAPI::errorCheck(studio_system_->release(), 4);
}

void AudioAPI::init() {
	AudioAPI::errorCheck(FMOD::Studio::System::create(&studio_system_), 0);
	AudioAPI::errorCheck(studio_system_->initialize(16, FMOD_STUDIO_INIT_LIVEUPDATE, FMOD_INIT_NORMAL, NULL), 1);

	AudioAPI::errorCheck(studio_system_->getLowLevelSystem(&low_system_), 2);
	AudioAPI::errorCheck(low_system_->set3DSettings(1.0, DISTANCEFACTOR, 1.0f), 23);
}

void AudioAPI::update() {

	std::vector<Mchannels_::iterator> stopped_channels;

	for (auto it = channels_.begin(), itEnd = channels_.end(); it != itEnd; ++it)
	{
		bool channel_playing = false;
		it->second->isPlaying(&channel_playing);
		if (!channel_playing)
		{
			stopped_channels.push_back(it);
		}
	}

	for (auto& it : stopped_channels)
	{
		channels_.erase(it);
	}

	AudioAPI::errorCheck(studio_system_->update(), 5);
}

void AudioAPI::loadSound(const std::string& filename, bool loop, bool d3) {

	//Check if the sound already exists
	auto sound_it = sounds_.find(filename);
	if (sound_it != sounds_.end()) {
		return;
	}

	FMOD::Sound *tmp_sound = nullptr;
	AudioAPI::errorCheck(low_system_->createSound(filename.c_str(), FMOD_DEFAULT, nullptr, &tmp_sound), 6);

	if (!loop) {
		AudioAPI::errorCheck(tmp_sound->setMode(FMOD_LOOP_OFF), 7);
	}
	
	if (d3) {
		AudioAPI::errorCheck(tmp_sound->setMode(FMOD_3D), 8);
		AudioAPI::errorCheck(tmp_sound->set3DMinMaxDistance(0.35f * DISTANCEFACTOR, 100000000.0f * DISTANCEFACTOR), 22);
		AudioAPI::errorCheck(tmp_sound->setMode(FMOD_3D_INVERSEROLLOFF), 25);
	}
	else {
		AudioAPI::errorCheck(tmp_sound->setMode(FMOD_2D), 9);
	}

	if (tmp_sound) {
		sounds_[filename] = tmp_sound;
	}

}

void AudioAPI::playSound(const std::string& filename, const FVector sound_pos, float volume) {


	auto sound_it = sounds_.find(filename);

	if (sound_it == sounds_.end()) {

		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("SOUND NOT CHARGED"));
		return;
	}

	FMOD::Channel *tmp_channel = nullptr;

	AudioAPI::errorCheck(low_system_->playSound(sound_it->second, nullptr, true, &tmp_channel), 10);

	if (tmp_channel) {

		FMOD_MODE tmp_mod;
		sound_it->second->getMode(&tmp_mod);

		if (tmp_mod & FMOD_3D) {
			FMOD_VECTOR sound_pos_ = FVectorToFMOD(sound_pos);
			AudioAPI::errorCheck(tmp_channel->set3DAttributes(&sound_pos_, nullptr), 11);
		}
		AudioAPI::errorCheck(tmp_channel->setVolume(volume), 12);
		AudioAPI::errorCheck(tmp_channel->setPaused(false), 13);

		channels_[next_channel_id_] = tmp_channel;
		next_channel_id_++;
	}
}

void AudioAPI::loadBank(const std::string& filename) {

	auto bank_it = banks_.find(filename);

	if (bank_it != banks_.end()) {
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("BANK STILL LOADED"));
		return;
	}

	FMOD::Studio::Bank *tmp_bank;
	AudioAPI::errorCheck(studio_system_->loadBankFile(filename.c_str(), FMOD_STUDIO_LOAD_BANK_NORMAL, &tmp_bank), 14);

	if (tmp_bank) {
		banks_[filename] = tmp_bank;
	}
	else {
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("BANK NOT FOUND"));
	}
}

void AudioAPI::loadEvent(const std::string& filename) {

	auto event_it = events_.find(filename);
	if (event_it != events_.end()) {
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("EVENT STILL LOADED"));
		return;
	}

	FMOD::Studio::EventDescription *tmp_event_desc = nullptr;
	AudioAPI::errorCheck(studio_system_->getEvent(filename.c_str(), &tmp_event_desc), 15);

	if (tmp_event_desc) {
		FMOD::Studio::EventInstance *tmp_event_ins = nullptr;
		AudioAPI::errorCheck(tmp_event_desc->createInstance(&tmp_event_ins), 16);
		if (tmp_event_ins) {
			events_[filename] = tmp_event_ins;
		}
	}
}

void AudioAPI::playEvent(const std::string& filename) {

	auto event_it = events_.find(filename);

	if (event_it == events_.end()) {
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("EVENT NOT CHARGED"));
		return;
	}

	event_it->second->start();
}

void AudioAPI::stopEvent(const std::string& filename, bool fade_out) {
	auto event_it = events_.find(filename);

	if (event_it == events_.end()) {
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("EVENT NOT CHARGED"));
		return;
	}

	if (fade_out) {
		AudioAPI::errorCheck(event_it->second->stop(FMOD_STUDIO_STOP_ALLOWFADEOUT), 17);
	}
	else {
		AudioAPI::errorCheck(event_it->second->stop(FMOD_STUDIO_STOP_IMMEDIATE), 18);
	}
}

bool AudioAPI::isEventPlaying(const std::string& filename) {
	auto event_it = events_.find(filename);

	if (event_it == events_.end()) {
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("EVENT NOT CHARGED"));
		return false;
	}

	FMOD_STUDIO_PLAYBACK_STATE *tmp_state = NULL;

	if (event_it->second->getPlaybackState(tmp_state) == FMOD_STUDIO_PLAYBACK_PLAYING) {
		return true;
	}
	return false;
}

void AudioAPI::getEventParameter(const std::string& filename, const std::string& parameter_name, float *parameter) {
	auto event_it = events_.find(filename);

	if (event_it == events_.end()) {
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("EVENT NOT CHARGED"));
		return;
	}

	FMOD::Studio::ParameterInstance *tmp_parameter = nullptr;

	AudioAPI::errorCheck(event_it->second->getParameter(parameter_name.c_str(), &tmp_parameter), 19);
	AudioAPI::errorCheck(event_it->second->getParameterValue(parameter_name.c_str(), parameter, NULL), 20);
}

void AudioAPI::setEventParameter(const std::string& filename, const std::string& parameter_name, float parameter_value) {
	auto event_it = events_.find(filename);

	if (event_it == events_.end()) {
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("EVENT NOT CHARGED"));
		return;
	}

	FMOD::Studio::ParameterInstance *tmp_parameter = nullptr;

	AudioAPI::errorCheck(event_it->second->getParameter(parameter_name.c_str(), &tmp_parameter), 21);
	AudioAPI::errorCheck(event_it->second->setParameterValue(parameter_name.c_str(), parameter_value), 22);
}



FMOD_VECTOR AudioAPI::FVectorToFMOD(const FVector vector) {
	FMOD_VECTOR fmod_vector;

	fmod_vector.x = vector.X;
	fmod_vector.y = vector.Y;
	fmod_vector.z = vector.Z;

	return fmod_vector;
}

void AudioAPI::setChannel3DAttribs(int channel_id, const FVector sound_pos, const FVector sound_velocity) {

	auto channels_it = channels_.find(channel_id);

	if (channels_it == channels_.end()) {
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("CHANNEL NOT FOUND"));
		return;
	}

	FMOD_VECTOR sound_pos_ = FVectorToFMOD(sound_pos);
	FMOD_VECTOR sound_velocity_ = FVectorToFMOD(sound_velocity);

	errorCheck(channels_it->second->set3DAttributes(&sound_pos_, &sound_velocity_), 14);

}

void AudioAPI::set3DListener(const FVector listenerpos, const FVector forward, const FVector up) {

	FMOD_VECTOR listener_pos_ = FVectorToFMOD(listenerpos);
	FMOD_VECTOR forward_ = FVectorToFMOD(forward);
	FMOD_VECTOR up_ = FVectorToFMOD(up);

	AudioAPI::errorCheck(low_system_->set3DListenerAttributes(0, &listener_pos_, 0, &forward_, &up_), 25);
}

int AudioAPI::errorCheck(FMOD_RESULT result, int id) {

	if (result != FMOD_OK) {

		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("FMOD ERROR") + FString::SanitizeFloat(id));

		return 1;
	}

	return 0;
}


