// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE4III_borrajomeGameMode.generated.h"

UCLASS(minimalapi)
class AUE4III_borrajomeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUE4III_borrajomeGameMode();
};



